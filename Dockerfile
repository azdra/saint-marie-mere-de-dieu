FROM node:alpine AS runner

# Create app directory
WORKDIR /app

# Installing dependencies
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install

COPY . ./

COPY docker/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

EXPOSE 3000
ENTRYPOINT ["docker-entrypoint"]
CMD ["npm", "run", "dev"]
