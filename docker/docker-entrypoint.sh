#!/bin/sh
set -e

echo '1'

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- node "$@"
fi

echo '2'

npm install

echo '3'

exec "$@"
