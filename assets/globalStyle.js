import { createGlobalStyle } from 'styled-components'

import FjallaOneWoff from '../assets/font/FjallaOneWoff.woff'
import FjallaOneWoff2 from '../assets/font/FjallaOneWoff2.woff2'
import RobotoWoff from '../assets/font/RobotoWoff.woff'
import RobotoWoff2 from '../assets/font/RobotoWoff2.woff2'

export const GlobalStyle = createGlobalStyle`

    @font-face {
        font-family: 'FjallaOne';
        src: local('FjallaOne'), local('FjallaOne'),
        url(${ FjallaOneWoff }) format('woff'),
        url(${ FjallaOneWoff2 }) format('woff2');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'Roboto';
        src: local('Roboto'), local('Roboto'),
        url(${ RobotoWoff }) format('woff'),
        url(${ RobotoWoff2 }) format('woff2');
        font-weight: 300;
        font-style: normal;
    }

    * {
        padding: 0px;
        margin: 0px;
    }

    html {
        font-size: 100%;
        @media (max-width: 768px) {
            font-size: 112.5%;
        };
    }

    body {
        background-color: #ffffff;
    }

    header,
    article, 
    aside, 
    footer,   
    nav, 
    section,
    figure {
	    display: block;
    }

    h1 {
        font-family: FjallaOne, helvetica, arial, sans-serif;
        font-size: 2em;
        font-weight: normal;
    }

    ul,
    ol {
        list-style: none;
    }
`