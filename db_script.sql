START TRANSACTION;

-- ============================================================
--   Suppression et création de la base de données 
-- ============================================================

DROP DATABASE IF EXISTS smmdd;
CREATE DATABASE smmdd;
USE smmdd;

-- ============================================================
--   Création des tables                                
-- ============================================================

CREATE TABLE role (
	role_id		INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(50) NOT NULL,
	CONSTRAINT PK_role PRIMARY KEY (role_id),
	UNIQUE KEY (name)
);

CREATE TABLE user (
	user_id		INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(255) NOT NULL,
	surname		VARCHAR(255) NOT NULL,
	email		VARCHAR(255) NOT NULL,
	password 	VARCHAR(255) NOT NULL,
	created_at	DATE,
	updated_at 	DATE,
	role_id		INT,
	CONSTRAINT PK_user PRIMARY KEY (user_id),
	CONSTRAINT FK_user_role FOREIGN KEY (role_id) REFERENCES role(role_id),
	UNIQUE KEY (email)
);

CREATE TABLE ufr (
	ufr_id 		INT NOT NULL AUTO_INCREMENT,
	name 		VARCHAR(255) NOT NULL,
	CONSTRAINT PK_ufr PRIMARY KEY (ufr_id),
	UNIQUE KEY (name)
);

CREATE TABLE ufr_user (
	ufr_id		INT NOT NULL,
	user_id		INT NOT NULL,
	CONSTRAINT PK_ufr_user PRIMARY KEY (ufr_id, user_id),
	CONSTRAINT FK_ufr_user_ufr FOREIGN KEY (ufr_id) REFERENCES ufr(ufr_id),
	CONSTRAINT FK_ufr_user_user FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE speciality (
	speciality_id 	INT NOT NULL AUTO_INCREMENT,
	name			VARCHAR(255) NOT NULL,
	ufr_id			INT NOT NULL,
	CONSTRAINT PK_speciality PRIMARY KEY (speciality_id),
	CONSTRAINT FK_speciality_ufr FOREIGN KEY (ufr_id) REFERENCES ufr(ufr_id),
	UNIQUE KEY (name)
);

CREATE TABLE promotion (
	promotion_id 	INT NOT NULL AUTO_INCREMENT,
	level			VARCHAR(2) NOT NULL,
	number			INT NOT NULL,
	start_date 		DATE,
	end_date		DATE,
	speciality_id	INT NOT NULL,
	CONSTRAINT PK_promotion PRIMARY KEY (promotion_id),
	CONSTRAINT FK_promotion_speciality FOREIGN KEY (speciality_id) REFERENCES speciality(speciality_id)
);

CREATE TABLE promotion_user (
	promotion_id	INT NOT NULL,
	user_id			INT NOT NULL,
	CONSTRAINT PK_promotion_user PRIMARY KEY (promotion_id, user_id),
	CONSTRAINT FK_promotion_user_promotion FOREIGN KEY (promotion_id) REFERENCES promotion(promotion_id),
	CONSTRAINT FK_promotion_user_user FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE course (
	course_id		INT NOT NULL AUTO_INCREMENT,
	name 			VARCHAR(255) NOT NULL,
	start_time		DATETIME,
	end_time		DATETIME,
	promotion_id	INT NOT NULL,
	user_id			INT,
	CONSTRAINT PK_course PRIMARY KEY (course_id),
	CONSTRAINT FK_course_promotion FOREIGN KEY (promotion_id) REFERENCES promotion(promotion_id),
	CONSTRAINT FK_course_user FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE status (
	status_id	INT NOT NULL AUTO_INCREMENT,
	name		VARCHAR(100) NOT NULL,
	CONSTRAINT PK_status PRIMARY KEY (status_id),
	UNIQUE KEY (name)
);

CREATE TABLE absence (
	absence_id	INT NOT NULL AUTO_INCREMENT,
	course_id	INT NOT NULL,
	user_id		INT NOT NULL,
	status_id	INT NOT NULL,
	CONSTRAINT PK_absence PRIMARY KEY (absence_id),
	CONSTRAINT FK_absence_course FOREIGN KEY (course_id) REFERENCES course(course_id),
	CONSTRAINT FK_absence_user FOREIGN KEY (user_id) REFERENCES user(user_id),
	CONSTRAINT FK_absence_status FOREIGN KEY (status_id) REFERENCES status(status_id)
);

-- ============================================================
--   Insertion des données                               
-- ============================================================

INSERT INTO role (role_id, name) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_PROFESSOR'),
(3, 'ROLE_SECRETARY'),
(4, 'ROLE_STUDENT');

INSERT INTO ufr (ufr_id, name) VALUES
(1, 'Foreign languages'),
(2, 'Human sciences'),
(3, 'Medicine');

INSERT INTO speciality (speciality_id, name, ufr_id) VALUES
(1, 'French', 1),
(2, 'Dutch', 1),
(3, 'Algerian', 1),
(4, 'Psychology', 2),
(5, 'Sociology', 2),
(6, 'Anthropology', 2),
(7, 'Philosophy', 2),
(8, 'Gynecology', 3),
(9, 'Neurology', 3),
(10, 'Cardiology', 3);

INSERT INTO status (status_id, name) VALUES
(1, 'To justify'),
(2, 'Pending'),
(3, 'Justified'),
(4, 'Unjustified');

INSERT INTO promotion (promotion_id, level, number, start_date, end_date, speciality_id) VALUES
(1, 'L1', 3, '2021-09-01', '2022-09-01', 3),
(2, 'L3', 2, '2021-09-01', '2022-09-01', 7),
(3, 'M2', 6, '2021-09-01', '2022-09-01', 10);

INSERT INTO user (user_id, name, surname, email, password, created_at, updated_at, role_id) VALUES
(1, 'Kévin', 'Wolff', 'keke38@gmail.com', 'lescopaingdabord', '2021-09-01', '2021-09-01', 4),
(2, 'Lisa', 'Michallon', 'liswag@om.dz', 'didleforever', '2021-09-02', '2021-09-02', 2),
(3, 'Baptiste', 'Le Blanc', 'gandalf-master@zebi.com', 'nsmleblizard', '2021-09-02', '2021-09-02', 4),
(4, 'Thomasse', 'Chachapuis', 'dj-chachapui@live.fr', 'NFUE_Zhf\"é)àHUZFObjfe', '2021-08-31', '2021-08-31', 1),
(5, 'Maria', 'Di Thomasso', '01clean@corp.com', 'panamaforever', '2021-09-01', '2021-09-01', 3);

INSERT INTO promotion_user (promotion_id, user_id) VALUES (1,3), (3,1);

INSERT INTO ufr_user (ufr_id, user_id) VALUES (1, 2), (1, 5), (2, 4);

INSERT INTO course (course_id, name, start_time, end_time, promotion_id, user_id) VALUES
(1, 'Histoire de la philosophie', '2021-09-02 13:30:00', '2021-09-02 15:30:00', 1, 4), 
(2, 'Matérialisme dialectique', '2021-09-02 15:30:00', '2021-09-02 17:30:00', 1, 4);

INSERT INTO absence (absence_id, course_id, user_id, status_id) VALUES (1, 1, 3, 1), (2, 2, 3, 2);

COMMIT;