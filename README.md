# Getting Started

## Start the project

Copy and past the .env.local file.

```bash
$ cp .env .env.local
```
Install dependencies

```bash
npm install
```

Fill in the different parameters.

```bash
$ docker-compose build
$ docker-compose up
```

### Exec the creation database script in the Docker db container.

```bash
cat db_script.sql | docker-compose exec -T db mysql -u root -pSUvNdWxtB9WmA4KF smmdd
```

# Enjoy

Mais ne sèche pas l'école !
