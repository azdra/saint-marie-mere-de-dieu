import axios, {AxiosPromise, Method} from 'axios';

export type MakeRequest = {
  method: Method,
  url: string,
  baseURL?: string,
  data?: any,
}

export const makeRequest = (config: MakeRequest): AxiosPromise<any> => {
  return axios({
    method: config.method,
    url: config.url,
    baseURL: config.baseURL ?? 'http://localhost:3000/api/',
    data: config.data
  })
}
