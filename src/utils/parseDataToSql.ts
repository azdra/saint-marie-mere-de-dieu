export const parseDataToSql = (data: any): { query: string; values: any } => {
  let str = '';
  let i = 0;
  const newValues = {};

  for (const dataKey in data) {
    if (Object.prototype.hasOwnProperty.call(data, dataKey)) {
      if (i > 0 || i < (data.length-1)) str += 'NEED';
      str += `${dataKey} = :${dataKey}`;
      Object.assign(newValues, {[dataKey]: "'"+data[dataKey]+"'"})
      i++;
    }
  }

  return {
    query: str,
    values: newValues
  };
}
