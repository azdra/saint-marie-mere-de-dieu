export const parseParameters = (query: string, values?: object): string => {
  // const regex = /(?<=[:])\S+/gim;
  // const match = newQuery.match(regex);
  let newQuery = query;

  if (values) {
    for (let value in values) {
      if (Object.prototype.hasOwnProperty.call(values, value)) {
        // TODO Fix @ts-ignore
        // @ts-ignore
        newQuery = newQuery.replace(`:${value}`, values[value])
      }
    }
  }
  return newQuery;
}
