import mariadb, {Pool, PoolConnection} from 'mariadb';

export class DbConnect {
  private readonly host?: string;
  private readonly user?: string;
  private readonly password?: string;
  private readonly database?: string;
  private readonly port?: number;

  constructor() {
    this.host = process.env.MYSQL_HOST;
    this.user = process.env.MYSQL_USER;
    this.password = process.env.MYSQL_PASSWORD;
    this.database = process.env.MYSQL_DATABASE;
    this.port = Number(process.env.MYSQL_PORT);


    /*this.createConnection().getConnection().then(() => {
      console.log('CONNECTED')
    })*/
    //   .then(() => console.log('connection to the db'))
    //   .catch((e) => console.log(e));
  }

  private createConnection = (): Pool => mariadb.createPool({
    host: this.host,
    user: this.user,
    password: this.password,
    database: this.database,
    port: this.port,
    connectionLimit: 1,
  });

  protected getConnection = (): Pool => {
    return this.createConnection();
  };
}
