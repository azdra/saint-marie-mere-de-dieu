import styled from 'styled-components';

const Main = styled.main`
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 2fr;
    max-width: 1280px;
    margin: 0 auto;
`;

export default Main;