import styled from 'styled-components';

const Article = styled.article`
    grid-column: 1/2;
    grid-row: 2/3;
    padding: 10px;
`

export default Article;