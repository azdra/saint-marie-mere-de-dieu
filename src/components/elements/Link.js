import Link from 'next/link';
import styled from 'styled-components';

const StyledA = styled.a`
  display: block;
`

const NextLink = ({ href, className, children }) => (
  <Link href={ href } passHref>
    <StyledA className={ className }>{ children }</StyledA>
  </Link>
);

export default NextLink;