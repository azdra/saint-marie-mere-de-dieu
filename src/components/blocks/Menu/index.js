import styled from 'styled-components';
import { List, ListItem, MenuItem, MenuLink, MenuTitle } from './style';

import AbsenceSvg from '../../../../assets/image/icon-absence.svg';
import AttendanceSvg from '../../../../assets/image/icon-attendance.svg';
import CoursesSvg from '../../../../assets/image/icon-courses.svg';
import PromotionSvg from '../../../../assets/image/icon-promotion.svg';
import SpecialiteSvg from '../../../../assets/image/icon-specialite.svg';
import UfrSvg from '../../../../assets/image/icon-ufr.svg';

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 50px;
    height: 300px;
`

const MENU_OPTIONS = [
    {
        route: '/absence',
        icon: <AbsenceSvg/>,
        label: 'Absence'
    },
    {
        route: '#',
        icon: <AttendanceSvg/>,
        label: 'Attendance'
    },
    {
        route: '#',
        icon: <CoursesSvg/>,
        label: 'Courses'
    },
    {
        route: '#',
        icon: <PromotionSvg/>,
        label: 'Promotion'
    },
    {
        route: '#',
        icon: <SpecialiteSvg/>,
        label: 'Specialité'
    },
    {
        route: '#',
        icon: <UfrSvg/>,
        label: 'UFR'
    },
];

const Menu = () => {

    return (
        <Container>
            <List>
                {MENU_OPTIONS.map(({route, icon, label}) => (
                    <ListItem key={label}>
                        <MenuLink href={route}>
                        <MenuItem>
                            {icon}
                            <MenuTitle>
                                {label}
                            </MenuTitle>
                        </MenuItem>
                        </MenuLink>
                    </ListItem>
                ))}
            </List>
        </Container>
    );
};

export default Menu;