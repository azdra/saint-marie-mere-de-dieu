import styled from 'styled-components';
import Link from '../../elements/Link';

const List = styled.ul`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: repeat(2, 1fr);
    list-style: none;
`

const ListItem = styled.li`
    padding: 15px;
    margin: 10px 30px;
    width: 100px;
    height: 100px;
    cursor: pointer;

    &:hover {
        p {
            display: block;
        }

        svg {
            top: -5px;
        }
    }

    svg {
        position: absolute;
        top: 0;
        width: 75px;
        height: 75px;
        transition: 200ms;
    }
`

const MenuLink = styled(Link)`
    color: #000000;
    text-decoration: none;
`

const MenuItem = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
`

const MenuTitle = styled.p`
    position: absolute;
    top: 85px;
    display: none;
    font-family: FjallaOne, helvetica, arial, sans-serif;
    letter-spacing: 0.25em;
`

export { List, ListItem, MenuItem, MenuLink, MenuTitle };