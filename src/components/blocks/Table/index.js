import styled from 'styled-components';

const Table = styled.table`
    border-collapse: collapse;
    margin: 25px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    width: 100%;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);

    th,
    td {
        padding: 12px 15px;
    }

    thead tr {
        background-color: #78909c;
        color: #ffffff;
        text-align: left;
    }

    tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    tbody tr:last-of-type {
        border-bottom: 2px solid #78909c;
    }

    tbody tr.active-row {
        font-weight: bold;
        color: #78909c  ;
    }
`

export default Table;