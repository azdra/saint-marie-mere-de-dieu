import styled from 'styled-components';

const Nav = styled.nav`
    display: flex;
    flex-wrap: wrap;
    margin-top: 15px;
`

const NavItem = styled.p`
    margin-left: 35px;
    font-size: 1.25em;
    font-weight: normal;
    font-family: Roboto, helvetica, arial, sans-serif;
    cursor: pointer;

    &:first-child {
        margin-left: 0;
    }
`

const Title = styled.h1`
    padding-bottom: 3px;
    width: max-content;
    font-family: FjallaOne, helvetica, arial, sans-serif;
    border-bottom: 2px solid #000000;
`

const Wrapper = styled.div`
    display: flex;
    padding: 5px;
`

const Form = styled.form`
    background-color: lightcoral;
    margin-top: 20px;
`

const Label = styled.label`
    margin-right: 10px;
`

const InputText = styled.input.attrs(props => ({
    type: 'text',
  }))``

const InputDate = styled.input.attrs(props => ({
    type: 'date',
}))``

const Submit = styled.input.attrs(props => ({
    type: 'submit',
}))``

Form.Nav = Nav;
Form.NavItem = NavItem;
Form.Title = Title;
Form.Wrapper = Wrapper;
Form.Label = Label;
Form.InputText = InputText;
Form.InputDate = InputDate;
Form.Submit = Submit;

export default Form;