import styled from 'styled-components';
import BrandLogo from '../../../../assets/image/brand.svg';
import Link from '../../elements/Link';

const BrandContainer = styled.div`
    margin-left: 20px;
`

const Logo = styled(BrandLogo)`
    display: block;
    width: 40px;
    height: 40px;
`

const Brand = styled.h1`
    margin-left: 20px;
`

const BrandLink = styled(Link)`
    display: flex;
    flex-wrap: wrap;
    align-items: flex-end;
    text-decoration: none;
    color: #000000;
`

const List = styled.ul`
    display: flex;
    flex-wrap: wrap;
    align-items: flex-end;
    margin-right: 20px;
`

const ListItem = styled.li`
    margin-right: 20px;
    font-family: FjallaOne, helvetica, arial, sans-serif;

    &:last-child {
        margin-right: 0;
    }
`

const ListLink = styled(Link)`
    text-decoration: none;
    color: #000000;
`

export { BrandContainer, Logo, Brand, BrandLink, List, ListItem, ListLink };