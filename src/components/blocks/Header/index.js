import styled from 'styled-components';

import { BrandContainer, Logo, Brand, BrandLink, List, ListItem, ListLink } from './style';

const Container = styled.div`
    grid-column: 1/2;
    grid-row: 1/2;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: flex-end;
    padding: 10px 0 5px 0;
    margin: 0 auto;
    max-width: 1280px;
`

const Header = () => {

    return (
        <Container>
            <BrandContainer>
                <BrandLink href='/'>
                    <Logo/>
                    <Brand>SMMDD</Brand>
                </BrandLink>
            </BrandContainer>
            <List>
                <ListItem>
                    <ListLink href='#'>
                            <p>Menu 1</p>
                    </ListLink>
                </ListItem>
                <ListItem>
                    <ListLink href='#'>
                            <p>Menu 2</p>
                    </ListLink>
                </ListItem>
                <ListItem>
                    <ListLink href='#'>
                            <p>Menu 3</p>
                    </ListLink>
                </ListItem>
            </List>
        </Container>
    )
};

export default Header;