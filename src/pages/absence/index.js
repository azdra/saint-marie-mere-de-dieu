import { Fragment, useState, useEffect } from 'react';
import styled from 'styled-components';
import { makeRequest } from '../../utils/makeRequest';
import Main from '../../components/elements/Main';
import Article from '../../components/elements/Article';
import Header from '../../components/blocks/Header';
import Form from '../../components/blocks/Form';
import Table from '../../components/blocks/Table';

const StyledArticle = styled(Article)`
    padding-top: 30px;
`

const AbsencePage = () => {
  const [ selectedForm, setSelectedForm ] = useState('home');
  const [ absences, setAbsences ] = useState([]);
  const [ courses, setcourses ] = useState([]);
  const [ students, setStudents ] = useState([]);
  const absencesStatus = {
    1: 'à justifier',
    2: 'en traitement',
    3: 'justifier',
    4: 'injustifier'
  };
  const tableHeader = ['ID', 'nom', 'prénom', 'cours', 'status'];

  useEffect( async () => {
    const resAbsence = await makeRequest({
      url: '/absence',
      method: 'GET',
    });

    const resCourse = await makeRequest({
      url: '/course',
      method: 'GET',
    });

    const resStudent = await makeRequest({
      url: '/user',
      method: 'GET',
    });

    setAbsences(resAbsence.data);
    setcourses(resCourse.data);
    setStudents(resStudent.data);
  }, []);

  return (
    <Fragment>
      <Header/>
      <Main>
        <StyledArticle>
            <Form.Title>
                Recherche d'absences
            </Form.Title>
            <Form.Nav>
                <Form.NavItem onClick={ () => { setSelectedForm('home') } }>Accueil</Form.NavItem>
                <Form.NavItem onClick={ () => { setSelectedForm('promotion') } }>Par promotion</Form.NavItem>
                <Form.NavItem onClick={ () => { setSelectedForm('student') } }>Par élève</Form.NavItem>
                <Form.NavItem onClick={ () => { setSelectedForm('date') } }>Par date</Form.NavItem>
            </Form.Nav>
            { (selectedForm === 'home' && absences.length > 0 && courses.length > 0 && students.length > 0) &&
            <Table>
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Nom</td>
                  <td>Prénom</td>
                  <td>Cours</td>
                  <td>Statut</td>
                </tr>
              </thead>
              <tbody>
                { absences.map(absence => 
                    <tr key={ absence.absence_id }>
                    <td>{ absence.absence_id }</td>
                    <td>{ students[(absence.user_id - 1)].surname }</td>
                    <td>{ students[(absence.user_id - 1)].name }</td>
                    <td>{ courses[(absence.course_id - 1)].name }</td> 
                    <td>{ absencesStatus[absence.status_id] }</td>
                    </tr>  
                ) }
              </tbody>
          </Table>
            }
        </StyledArticle>
      </Main>
    </Fragment>
  )
};

export default AbsencePage;
