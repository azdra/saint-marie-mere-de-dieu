import type { AppProps } from 'next/app';
import { Fragment } from 'react';
import { GlobalStyle } from '../../assets/globalStyle';

const Application = ({ Component, pageProps }: AppProps) => {

  return (
    <Fragment>
      <GlobalStyle/>
      <Component {...pageProps} />
    </Fragment>
  )
}

export default Application;
