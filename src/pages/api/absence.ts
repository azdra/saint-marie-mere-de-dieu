import type { NextApiRequest, NextApiResponse } from 'next';
import { AbsenceRepository } from '../../repository/AbsenceRepository';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const absenceRepo = new AbsenceRepository();
  /*await userRepo.create({
    name: 'string',
    surname: 'string',
    email: 'string',
    password: 'string',
    created_at: 'Date',
    updated_at: 'Date',
    role_id: 'number',
  })*/
  const data = await absenceRepo.findAll();
  return res.status(200).json(data)
}
