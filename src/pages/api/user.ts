import type { NextApiRequest, NextApiResponse } from 'next';
import { UserRepository } from '../../repository/UserRepository';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const userRepo = new UserRepository();
  /*await userRepo.create({
    name: 'string',
    surname: 'string',
    email: 'string',
    password: 'string',
    created_at: 'Date',
    updated_at: 'Date',
    role_id: 'number',
  })*/
  const data = await userRepo.findAll();
  return res.status(200).json(data)
}
