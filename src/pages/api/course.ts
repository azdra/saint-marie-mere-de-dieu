import type { NextApiRequest, NextApiResponse } from 'next';
import { CourseRepository } from '../../repository/CourseRepository';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const courseRepo = new CourseRepository();
  /*await userRepo.create({
    name: 'string',
    surname: 'string',
    email: 'string',
    password: 'string',
    created_at: 'Date',
    updated_at: 'Date',
    role_id: 'number',
  })*/
  const data = await courseRepo.findAll();
  return res.status(200).json(data)
}
