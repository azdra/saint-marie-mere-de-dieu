import { Fragment } from 'react';
import type { NextPage } from 'next';
import Main from '../components/elements/Main';
import Article from '../components/elements/Article';
import Header from '../components/blocks/Header';
import Menu from '../components/blocks/Menu';

const Home: NextPage = () => {

  return (
    <Fragment>
      <Header/>
      <Main>
        <Article>
          <Menu/>
        </Article>
      </Main>
    </Fragment>
  )
};

export default Home;
