import {DbConnect} from "../components/dbConnect";
import {parseParameters} from "../utils/parseParameters";
import {parseDataToSql} from "../utils/parseDataToSql";

export type Entity =
  'absence' | 'attendance' |
  'course' | 'promotion' |
  'promotion_user' | 'role' |
  'speciality' | 'status' |
  'ufr' | 'ufr_user' |
  'user';

export interface Repository {
  entity: Entity;
}

export class BaseRepository extends DbConnect {
  private readonly _entity: Entity;

  public constructor(props: Entity) {
    super();
    this._entity = props;
  }

  public createQuery = (query: string, values?: any): Promise<void> => {
    return new Promise((resolve, reject) => {
      const newQuery = parseParameters(query, values);

      this.getConnection().query(newQuery).then((r) => {
        if (Object.prototype.hasOwnProperty.call(r, 'meta')) delete r.meta;
        return resolve(r);
      }).catch((e) => reject(e))
    })
  };

  public delete = (id: number) => {
    return this.createQuery(`DELETE FROM ${this._entity} WHERE ${this._entity}_id = :id`, {
      id: id
    })
  }

  public create = (data: any) => {
    const parsing = parseDataToSql(data);
    const queryParse = parsing.query.replace(/NEED/gi, ', ');

    return this.createQuery(
      `INSERT INTO ${this._entity} SET ${queryParse}`,
      parsing.values
    )
  }

  public update = (id: number, data: any) => {
    const parsing = parseDataToSql(data);
    const queryParse = parsing.query.replace(/NEED/gi, ', ');
    const values = Object.assign({id}, parsing.values);

    return this.createQuery(
      `UPDATE ${this._entity} SET ${queryParse} WHERE ${this._entity}_id = :id`,
      values
    )
  }

  public findAll = () => {
    return this.createQuery(`SELECT * FROM ${this._entity}`);
  }

  public findBy = (data: object) => {
    const parsing = parseDataToSql(data);
    const queryParse = parsing.query.replace(/NEED/gi, ' AND ');

    return this.createQuery(`SELECT * FROM ${this._entity} WHERE ${queryParse}`, data);
  };


  public findOneBy = (data: object) => {
    return new Promise((resolve) => {
      return this.findBy(data).then((r) => {
        if (Array.isArray(r)) return resolve(r[0]);
        return resolve(r);
      })
    })
  };
}
