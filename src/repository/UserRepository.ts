import {BaseRepository, Entity, Repository} from "./BaseRepository";

export type User =  {
  user_id?: number;
  name?: string;
  surname?: string;
  email?: string;
  password?: string;
  created_at?: Date;
  updated_at?: Date;
  role_id?: number;
}

export class UserRepository extends BaseRepository implements Repository {
  public entity: Entity = 'user';

  constructor() {
    super('user');
  }
}
